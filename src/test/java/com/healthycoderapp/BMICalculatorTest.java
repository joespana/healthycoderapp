package com.healthycoderapp;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

class BMICalculatorTest {

	private static int count = 0;
	private String environment = "dev";

	@AfterEach
	void afterEach() {
		System.out.println("Unit test #" + ++count + " was finished.\n=========================");
	}

	@Nested
	@DisplayName("IsDietRecommended")
	class IsDietRecommendedTests {

		@ParameterizedTest
		@ValueSource(doubles = { 89.0, 95.0, 110.0 })
		@DisplayName("Sample Name")
		void shouldReturnTrueWhenDietRecommendedValueSource(Double coderWeight) {

			// given/arrange
			double weight = coderWeight;
			double height = 1.72;

			// when/act
			boolean recommended = BMICalculator.isDietRecommended(weight, height);

			// then/assert
			assertTrue(recommended);

		}

		@ParameterizedTest(name = "weight={0}, height={1}")
		@CsvSource(value = { "89.0, 1.72", "95.0, 1.80", "110.0, 1.65" })
		void shouldReturnTrueWhenDietRecommendedCsv(Double coderWeight, Double coderHeight) {

			// given/arrange
			double weight = coderWeight;
			double height = coderHeight;

			// when/act
			boolean recommended = BMICalculator.isDietRecommended(weight, height);

			// then/assert
			assertTrue(recommended);

		}

		@ParameterizedTest(name = "weight={0}, height={1}")
		@CsvFileSource(resources = "/diet-recommended-input-data.csv", numLinesToSkip = 1)
		void shouldReturnTrueWhenDietRecommendedCsvFile(Double coderWeights, Double coderHeights) {

			// given/arrange
			double weight = coderWeights;
			double height = coderHeights;

			// when/act
			boolean recommended = BMICalculator.isDietRecommended(weight, height);

			// then/assert
			assertTrue(recommended);

		}

		@Test
		void shouldReturnFalseWhenDietNotRecommended() {

			// given/arrange
			double weight = 50.0;
			double height = 1.92;

			// when/act
			boolean recommended = BMICalculator.isDietRecommended(weight, height);

			// then/assert
			assertFalse(recommended);

		}

		@Test
		void shouldThrowArithmeticExceptionWhenHeightZero() {

			// given/arrange
			double weight = 50.0;
			double height = 0.0;

			// when/act
			Executable executable = () -> BMICalculator.isDietRecommended(weight, height);

			// then/assert
			assertThrows(ArithmeticException.class, executable);

		}
	}

	@Nested
	@DisplayName("FindCoderWithWorstBMI")
	class FindCoderWithWorstBMITests {

		@Test
		void shouldReturnCoderWithWorstBMIWhenCoderListNotEmpty() {

			// given
			List<Coder> coders = new ArrayList<>();
			coders.add(new Coder(1.80, 60.0));
			coders.add(new Coder(1.82, 98.0));
			coders.add(new Coder(1.82, 64.7));

			// when
			Coder coderWorstBMI = BMICalculator.findCoderWithWorstBMI(coders);

			// then
			assertAll(() -> assertEquals(1.82, coderWorstBMI.getHeight()),
					() -> assertEquals(98.0, coderWorstBMI.getWeight()));
		}

		@Test
		void shouldReturnCoderWithWorstBMIIn50MSWhenCoderListHas10000Elements() {

			// given
			assumeTrue(BMICalculatorTest.this.environment.equals("prod"));

			List<Coder> coders = new ArrayList<>();
			for (int i = 0; i < 10000; i++) {
				coders.add(new Coder(1.0 + i, 10.0 + i));
			}

			// when
			Executable executable = () -> BMICalculator.findCoderWithWorstBMI(coders);

			// then
			assertTimeout(Duration.ofMillis(50), executable);
		}

		@Test
		void shouldReturnNullCoderWithWorstBMIWhenCoderListEmpty() {

			// given
			List<Coder> coders = new ArrayList<>();

			// when
			Coder coderWorstBMI = BMICalculator.findCoderWithWorstBMI(coders);

			// then
			assertNull(coderWorstBMI);
		}
	}

	@Nested
	@DisplayName("GetBMIScores")
	class GetBMIScoresTests {

		@Test
		@DisabledOnOs(OS.WINDOWS)
		void shouldReturnCorrectBMIScoreArrayWhenCoderListNotEmpty() {

			// given
			List<Coder> coders = new ArrayList<>();
			coders.add(new Coder(1.80, 60.0));
			coders.add(new Coder(1.82, 98.0));
			coders.add(new Coder(1.82, 64.7));

			double[] expected = { 18.52, 29.59, 19.53 };

			// when
			double[] bmiScores = BMICalculator.getBMIScores(coders);

			// then
			assertArrayEquals(expected, bmiScores);
		}
	}

}
