package com.healthycoderapp;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;

class DietPlannerTest {

	private DietPlanner dietPlanner;
	private static int count = 0;
	
	@BeforeEach
	void setup() {
		this.dietPlanner = new DietPlanner(20, 30, 50);
	}
	
	@BeforeAll
	static void beforeAll() {
		System.out.println("Beginning of DietPlanner unit testing.\n");
	}
	
	@AfterEach
	void afterEach() {
		System.out.println("Unit test #" + ++count + " was finished.\n=========================");
	}
	
	@AfterAll
	static void afterAll() {
		System.out.println("\nEnd of DietPlanner unit testing.");
	}
	
	@RepeatedTest(value = 10, name = RepeatedTest.LONG_DISPLAY_NAME)
	void shouldReturnCorrectDietPlanWhenCorrectCoder() {
		
		//given
		Coder coder = new Coder(1.82, 75.0, 26, Gender.MALE);
		DietPlan expected = new DietPlan(2202, 110, 73, 275);
		
		//when
		DietPlan actual = dietPlanner.calculateDiet(coder);
		
		//then
		assertAll(() -> assertEquals(expected.getCalories(), actual.getCalories()),
				() -> assertEquals(expected.getCarbohydrate(), actual.getCarbohydrate()),
				() -> assertEquals(expected.getFat(), actual.getFat()),
				() -> assertEquals(expected.getProtein(), actual.getProtein()));
	}
	
	

}
