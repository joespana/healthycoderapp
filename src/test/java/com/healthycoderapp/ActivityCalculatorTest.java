package com.healthycoderapp;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

public class ActivityCalculatorTest {

	@Test
	void shouldReturnBadWhenAvgBelow20() {

		// given
		int weeklyCardioMin = 40;
		int weeklyWorkouts = 1;

		// when
		String actual = ActivityCalculator.rateActivityLevel(weeklyCardioMin, weeklyWorkouts);

		// then
		assertEquals("bad", actual);
	}

	@Test
	void shouldReturnAverageWhenAvgBetween20And40() {

		// given
		int weeklyCardioMin = 100;
		int weeklyWorkouts = 1;

		// when
		String actual = ActivityCalculator.rateActivityLevel(weeklyCardioMin, weeklyWorkouts);

		// then
		assertEquals("average", actual);
	}

	@Test
	void shouldReturnGoodWhenAvgAbove40() {

		// given
		int weeklyCardioMin = 155;
		int weeklyWorkouts = 3;

		// when
		String actual = ActivityCalculator.rateActivityLevel(weeklyCardioMin, weeklyWorkouts);

		// then
		assertEquals("good", actual);
	}

	@Test
	void shouldThrowIllegalArgumentExceptionWhenInputBelowZero() {
		
		// given
		int weeklyCardioMin = 0;
		int weeklyWorkouts = -1;

		// when
		Executable executable = () -> ActivityCalculator.rateActivityLevel(weeklyCardioMin, weeklyWorkouts);

		// then
		assertThrows(IllegalArgumentException.class, executable);
	}
}
