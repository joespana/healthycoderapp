package com.healthycoderapp;

public class ActivityCalculator {
	
	private ActivityCalculator() {}

	public static String rateActivityLevel(int weeklyCardioMin, int weeklyWorkoutSessions) {
		
		if(weeklyWorkoutSessions < 0 || weeklyCardioMin < 0 ) { 
			throw new IllegalArgumentException(); 
		}
		
		int totalMins = (weeklyWorkoutSessions*45) + weeklyCardioMin;
		int avg = totalMins/7;
		
		String retValue = "";
		
		if(avg < 20) {
			retValue = "bad";
		}else if(avg >=20 && avg<40) {
			retValue = "average";
		}else if(avg>=40) {	
			retValue = "good";
		}
		
		return retValue;
	}
}
